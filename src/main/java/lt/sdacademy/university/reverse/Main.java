package lt.sdacademy.university.reverse;

public class Main {
    public static void main(String[] args) {
        int number = 1934993;

        System.out.println(reverse(number));
    }

    public static int reverse(int number) {
        int reverse = 0;
        while (number > 0) {
            reverse = reverse * 10 + number % 10;
            number = number / 10;
        }
        return reverse;
    }
}
