package lt.sdacademy.university.models.domain;

public enum Gender {
    MALE,
    FEMALE
}
