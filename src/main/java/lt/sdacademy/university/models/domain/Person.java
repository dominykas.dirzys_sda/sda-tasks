package lt.sdacademy.university.models.domain;

public class Person extends Human {
    private String firstName;
    private String lastName;

    public Person(Integer age, Gender gender, String firstName, String lastName) {
        super(age, gender);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String getTitle() {
        String parentGetTitle = super.getTitle();
        return parentGetTitle + " " + firstName + " " + lastName;
    }

    public String getTitle(String verify) {
        String parentGetTitle = super.getTitle();
        if(parentGetTitle.isEmpty()){
            return firstName + " " + lastName;
        } else {
            return parentGetTitle + " " + firstName + " " + lastName;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
