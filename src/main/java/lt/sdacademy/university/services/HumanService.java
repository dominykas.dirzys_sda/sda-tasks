package lt.sdacademy.university.services;

import lt.sdacademy.university.models.domain.Gender;
import lt.sdacademy.university.models.domain.Human;
import lt.sdacademy.university.models.domain.Person;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

interface Transformer {
    String Transform(String message);
}

public class HumanService {
    public String transformHuman(Human human, BiFunction<Human, Human, String> transformer) {
        return transformer.apply(human, human);
    }

    public String transformMessage(String message, Transformer transformer) {
        return transformer.Transform(message);
    }

    public List<Person> getHumans() {
        List<Person> humanList = new ArrayList<>();
        Person humanMale1 = new Person(20, Gender.MALE, "Petras", "Petrauskas");
        Person humanMale2 = new Person(30, Gender.MALE, "Kazys", "Bumblauskas");
        Person humanFemale1 = new Person(18, Gender.FEMALE, "Eglė", "Petrauskaitė");
        Person humanFemale2 = new Person(25, Gender.FEMALE, "Jolanta", "Nuostabaitė");
        humanList.add(humanMale1);
        humanList.add(humanMale2);
        humanList.add(humanFemale1);
        humanList.add(humanFemale2);
        return humanList;
    }

    public List<Human> filterByGender(Gender gender, List<Human> humans) {
        List<Human> result = new ArrayList<>();

        for (Human human : humans) {
            if (human.getGender().equals(gender)) {
                result.add(human);
            }
        }
        return result;
    }

    public Map<Gender, List<Human>> groupByGender(List<Human> humans) {
        Map<Gender, List<Human>> result = new HashMap<>();
        for (Human human : humans) {
            if (!result.containsKey(human.getGender())) {
                result.put(human.getGender(), new ArrayList<>());
            }
            result.get(human.getGender()).add(human);
        }
        return result;
    }

    public List<Human> filterByAge(Integer age, List<Human> humans) {
        return humans.stream()
                .filter(h -> h.getAge().equals(age))
                .collect(toList());
    }

    public List<String> getNamesByGender(Gender gender, List<Person> people) {
        return people.stream()
                .filter(p -> p.getGender().equals(gender))
                .map(p -> p.getFirstName())
                .collect(toList());
        /*List<String> names = new ArrayList<>();
        List<Person> filteredPersons = humans.stream()
                .filter(h -> h.getGender().equals(gender))
                .collect(toList());
        filteredPersons.stream().forEach(h -> names.add(h.getFirstName()));
        return names;*/
    }

    public Map<String, List<Person>> groupByName(List<Person> people) {
        return people.stream()
                .collect(groupingBy(Person::getFirstName));
    }

    public List<String> getOrderedSurnames(List<Person> people) {
        return people.stream()
                .filter(p -> p.getAge() >= 18)
                .map(p -> p.getLastName())
                .sorted()
                .collect(toList());
    }

    public List<Integer> getOrderedAges(List<Person> people) {
        return people.stream()
                .map(p -> p.getAge())
                .sorted()
                .collect(toList());
    }
    /*
    filterByGender(Gender gender, List<Human> humans)
    returns list of Humans filtered by gender passed as a param
    getTheOldest(List<Human> humans)
    returns oldest Human
    getTheYoungestMen(List<Human> humans)
    returns youngest Human
    filterYoung(List<Human> humans)
    returns list of adult humans
    groupByGender(List<Human>)
    returns Map where key is name and value - list of humans that gender
            groupByAge*/
}
