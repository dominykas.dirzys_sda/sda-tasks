package lt.sdacademy.university.exam;

import java.util.HashMap;
import java.util.Map;

public class Exam {
    String title;
    int grade;

    public Exam(String title, int grade) {
        this.title = title;
        this.grade = grade;
    }

    public Map<String, Integer> getResults(String title, int grade) {
        Map<String, Integer> result = new HashMap<>();
        result.put(title, grade);
        return result;
    }

    public void improveResults() {
        grade = grade + 1;
    }

    public void improveResults(int grade2) {
        grade = grade2;
    }
}
