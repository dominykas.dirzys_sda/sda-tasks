package lt.sdacademy.university.tasks.fundamentals;

public class ATask {

    /**
     * Method print the message to the console
     *  and returns it as a result
     */
    public String printAndGetMessage() {
        String message = getMessage();
        System.out.println(message);
        return message;
    }

    private String getMessage() {
        return "Message from the method";
    }
}
