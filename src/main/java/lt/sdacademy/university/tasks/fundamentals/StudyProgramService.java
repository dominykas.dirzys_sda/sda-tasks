package lt.sdacademy.university.tasks.fundamentals;

import java.util.ArrayList;
import java.util.List;
import lt.sdacademy.university.models.dto.StudyProgram;

public class StudyProgramService {

    public StudyProgram getStudyProgram() {
        return new StudyProgram();
    }

    public StudyProgram getStudyProgramWithTitle(String title) {
        StudyProgram program = getStudyProgram();
        program.setTitle(title);
        return program;
    }

    public StudyProgram getStudyProgramWithIdAndTitle(Integer id, String title) {
        StudyProgram program = getStudyProgramWithTitle(title);
        program.setId(id);
        return program;
    }

    public StudyProgram getStudyProgramWithModule(String moduleTitle) {
        List<String> modules = new ArrayList<>();
        modules.add(moduleTitle);
        StudyProgram program = getStudyProgram();
        program.setModules(modules);
        return program;
    }

    public List<StudyProgram> getStudyPrograms(String[] titles) {
        List<StudyProgram> studyPrograms = new ArrayList<>();
        for (String title : titles) {
            StudyProgram program = new StudyProgram();
            program.setTitle(title);
            studyPrograms.add(program);
        }
        return studyPrograms;
    }

    public StudyProgram getStudyProgramWithModules(String[] moduleTitles) {
        StudyProgram program = new StudyProgram();
        List<String> modules = new ArrayList<>();
        for (int i = 0; i < moduleTitles.length; i++) {
            modules.add(moduleTitles[i]);
        }
        program.setModules(modules);
        return program;
    }

    public StudyProgram getLastStudyProgram(StudyProgram[] studyPrograms) {
        return studyPrograms[studyPrograms.length - 1];
    }

    public String getLongestModuleTitle(StudyProgram studyProgram) {
        int length = 0;
        String longestModuleTitle = "";
        for (String module : studyProgram.getModules()) {
            if (module.length() > length) {
                longestModuleTitle = module;
                length = module.length();
            }
        }
        return longestModuleTitle;
    }
}
