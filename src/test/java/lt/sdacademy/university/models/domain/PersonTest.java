package lt.sdacademy.university.models.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    @Test
    void getTitle() {
        String expectedTitle = "12 MALE Petras Petrauskas";
        Person person = new Person(12, Gender.MALE, "Petras", "Petrauskas");

        String result = person.getTitle();

        assertEquals(expectedTitle, result);
    }
}