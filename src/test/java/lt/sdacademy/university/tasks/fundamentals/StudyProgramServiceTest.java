package lt.sdacademy.university.tasks.fundamentals;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lt.sdacademy.university.models.dto.StudyProgram;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class StudyProgramServiceTest {

    private StudyProgramService studyProgramService;

    @BeforeEach
    void setUp() {
        studyProgramService = new StudyProgramService();
    }

    @Test
    public void getStudyProgram() {
        StudyProgram result = studyProgramService.getStudyProgram();

        assertNotNull(result);
    }

    @Test
    public void getStudyProgramWithTitle() {
        StudyProgram result = studyProgramService.getStudyProgramWithTitle("Study Program Title");

        assertNotNull(result);
        assertEquals("Study Program Title", result.getTitle());
    }

    @Test
    public void getStudyProgramWithIdAndTitle() {
        // TODO: complete test
    }

    @Test
    public void getStudyProgramWithModule() {
        StudyProgram result = studyProgramService.getStudyProgramWithModule("Module Title");

        assertNotNull(result);
        assertNotNull(result.getModules());
        assertEquals("Module Title", result.getModules().get(0));
    }

    @Test
    public void getStudyPrograms() {
        String[] studyProgramTitles = new String[]{
            "Study Program A",
            "Study Program B",
            "Study Program C"
        };

        List<StudyProgram> result = studyProgramService.getStudyPrograms(studyProgramTitles);

        assertNotNull(result);
        assertEquals("Study Program A", result.get(0).getTitle());
        assertEquals("Study Program B", result.get(1).getTitle());
        assertEquals("Study Program C", result.get(2).getTitle());
    }

    @Test
    public void getStudyProgramWithModules() {
        String[] moduleTitles = new String[]{
                "Module A",
                "Module B",
                "Module C"
        };

        StudyProgram result = studyProgramService.getStudyProgramWithModules(moduleTitles);

        assertNotNull(result);
        assertEquals("Module A", result.getModules().get(0));
        assertEquals("Module B", result.getModules().get(1));
        assertEquals("Module C", result.getModules().get(2));
    }

    @Test
    public void getLastStudyProgram() {
        StudyProgram[] studyPrograms = new StudyProgram[] {
                new StudyProgram(123, "Study Program A", new ArrayList<>()),
                new StudyProgram(124, "Study Program B", new ArrayList<>()),
                new StudyProgram(125, "Study Program C", new ArrayList<>())
        };

        StudyProgram result = studyProgramService.getLastStudyProgram(studyPrograms);

        assertEquals("Study Program C", studyPrograms[2].getTitle());
    }

    @Test
    public void getLongestModuleTitle() {
        String[] moduleTitles = new String[]{
                "Module A",
                "Module DEF",
                "Module BC"
        };
        StudyProgram studyProgram = new StudyProgram(123, "Study Program A", Arrays.asList(moduleTitles));

        String result = studyProgramService.getLongestModuleTitle(studyProgram);

        assertNotNull(result);
        assertEquals("Module DEF", result);
    }
}
