package lt.sdacademy.university.services;

import lt.sdacademy.university.models.domain.Gender;
import lt.sdacademy.university.models.domain.Human;
import lt.sdacademy.university.models.domain.Person;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class HumanServiceTest {

    @Test
    void getHumansTest() {
        List<Person> humanList = new ArrayList<>();
        humanList.add(new Person(20, Gender.MALE, "Petras", "Petrauskas"));
        humanList.add(new Person(30, Gender.MALE, "Kazys", "Bumblauskas"));
        humanList.add(new Person(18, Gender.FEMALE, "Eglė", "Petrauskaitė"));
        humanList.add(new Person(25, Gender.FEMALE, "Jolanta", "Nuostabaitė"));

        HumanService humanService = new HumanService();
        List<Person> result = humanService.getHumans();

        assertFalse(result.isEmpty());
    }

    @Test
    void groupByGenderTest() {
        List<Human> humanList = new ArrayList<>();
        humanList.add(new Person(20, Gender.MALE, "Petras", "Petrauskas"));
        humanList.add(new Person(30, Gender.MALE, "Kazys", "Bumblauskas"));
        humanList.add(new Person(18, Gender.FEMALE, "Eglė", "Petrauskaitė"));
        humanList.add(new Person(25, Gender.FEMALE, "Jolanta", "Nuostabaitė"));

        HumanService humanService = new HumanService();
        Map<Gender, List<Human>> result = humanService.groupByGender(humanList);

        assertNotNull(result);
        assertEquals(2, result.entrySet().size());
        assertEquals(2, result.get(Gender.MALE).size());
        assertEquals(2, result.get(Gender.FEMALE).size());
    }
    @Test
    void filterByAgeTest() {
        List<Human> humanList = new ArrayList<>();
        humanList.add(new Person(20, Gender.MALE, "Petras", "Petrauskas"));
        humanList.add(new Person(30, Gender.MALE, "Kazys", "Bumblauskas"));
        humanList.add(new Person(18, Gender.FEMALE, "Eglė", "Petrauskaitė"));
        humanList.add(new Person(25, Gender.FEMALE, "Jolanta", "Nuostabaitė"));

        HumanService humanService = new HumanService();
        List<Human> result = humanService.filterByAge(30, humanList);

        assertFalse(result.isEmpty());
        assertEquals(result.get(0).getAge(), 30);
    }

    @Test
    void getNamesByGenderTest() {
        List<Person> humanList = new ArrayList<>();
        humanList.add(new Person(20, Gender.MALE, "Petras", "Petrauskas"));
        humanList.add(new Person(30, Gender.MALE, "Kazys", "Bumblauskas"));
        humanList.add(new Person(18, Gender.FEMALE, "Eglė", "Petrauskaitė"));
        humanList.add(new Person(25, Gender.FEMALE, "Jolanta", "Nuostabaitė"));

        HumanService humanService = new HumanService();
        List<String> result = humanService.getNamesByGender(Gender.MALE, humanList);

        assertFalse(result.isEmpty());
        assertEquals(2, result.size());
        assertEquals("Petras", result.get(0));
    }

    @Test
    void groupByNameTest() {
        List<Person> humanList = new ArrayList<>();
        humanList.add(new Person(20, Gender.MALE, "Petras", "Petrauskas"));
        humanList.add(new Person(30, Gender.MALE, "Kazys", "Bumblauskas"));
        humanList.add(new Person(18, Gender.FEMALE, "Eglė", "Petrauskaitė"));
        humanList.add(new Person(25, Gender.FEMALE, "Jolanta", "Nuostabaitė"));

        HumanService humanService = new HumanService();
        Map<String, List<Person>> result = humanService.groupByName(humanList);

        assertNotNull(result);
        assertEquals(4, result.entrySet().size());
        assertEquals(1, result.get("Petras").size());
    }

    @Test
    void getOrderedSurnamesTest() {
        List<Person> humanList = new ArrayList<>();
        humanList.add(new Person(12, Gender.MALE, "Petras", "Petrauskas"));
        humanList.add(new Person(30, Gender.MALE, "Kazys", "Bumblauskas"));
        humanList.add(new Person(16, Gender.FEMALE, "Eglė", "Petrauskaitė"));
        humanList.add(new Person(25, Gender.FEMALE, "Jolanta", "Nuostabaitė"));

        HumanService humanService = new HumanService();
        List<String> result = humanService.getOrderedSurnames(humanList);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("Bumblauskas", result.get(0));
        assertEquals("Nuostabaitė", result.get(1));
    }


}